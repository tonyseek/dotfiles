#!/usr/bin/env bash

set -e
set -o pipefail
set -x

IJAVA_VERSION=1.3.0
IJAVA_DIST_NAME="ijava-${IJAVA_VERSION}.zip"
IJAVA_DIST_URL="https://github.com/SpencerPark/IJava/releases/download/v${IJAVA_VERSION}/${IJAVA_DIST_NAME}"

PIPX_LOCAL_VENVS="$(pipx environment | awk -F= '$1=="PIPX_LOCAL_VENVS" {print $2}')"
PIPX_JUPYTER_PYTHON="${PIPX_LOCAL_VENVS}/jupyter-core/bin/python3"

cd "$(mktemp -d)"
wget -O "${IJAVA_DIST_NAME}" -c "${IJAVA_DIST_URL}"
unzip "${IJAVA_DIST_NAME}"
exec "${PIPX_JUPYTER_PYTHON}" ./install.py --user

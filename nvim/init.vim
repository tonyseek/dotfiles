" install vim-plug
let s:plugblob = 'https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
let s:plugfile = expand('~/.local/share/nvim/site/autoload/plug.vim')
if !filereadable(s:plugfile)
  execute("!curl -fLo '".s:plugfile."' --create-dirs '".s:plugblob."'")
endif

call plug#begin()

" dialects
Plug 'rust-lang/rust.vim'
Plug 'cespare/vim-toml'
Plug 'asciidoc/vim-asciidoc'
Plug 'mitsuhiko/vim-jinja'
Plug 'google/vim-jsonnet'
Plug 'nathangrigg/vim-beancount'
Plug 'pangloss/vim-javascript'
Plug 'leafgarland/typescript-vim'
Plug 'MaxMEllon/vim-jsx-pretty'
Plug 'peitalin/vim-jsx-typescript'
Plug 'vim-scripts/OIL.vim'
Plug 'uarun/vim-protobuf'
Plug 'solarnz/thrift.vim'
Plug 'Orange-OpenSource/hurl', { 'rtp': 'contrib/vim' }

" extensions
Plug 'neoclide/coc.nvim', { 'branch': 'release' }
Plug 'nvim-lua/plenary.nvim'
Plug 'nvim-telescope/telescope.nvim'
Plug 'nvim-telescope/telescope-fzf-native.nvim', { 'do': 'make' }
Plug 'nvim-telescope/telescope-file-browser.nvim'
Plug 'nvim-treesitter/nvim-treesitter', { 'do': ':TSUpdate' }
Plug 'fannheyward/telescope-coc.nvim'
Plug 'LukasPietzschmann/telescope-tabs'
Plug 'editorconfig/editorconfig-vim'
Plug 'scrooloose/nerdcommenter'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-fugitive'
Plug 'mattn/emmet-vim'
Plug 'preservim/nerdtree', { 'on': 'NERDTreeToggle' }
Plug 'majutsushi/tagbar'
Plug 'iamcco/markdown-preview.nvim', {
  \ 'do': 'cd app && yarn install', 'for': 'markdown',
  \ 'on': ['MarkdownPreview', 'MarkdownPreviewStop', 'MarkdownPreviewToggle'] }
if has('mac')
  Plug 'mrjones2014/dash.nvim', { 'do': 'make install' }
else
  Plug 'KabbAmine/zeavim.vim'
endif

" themes
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'olimorris/onedarkpro.nvim'
Plug 'norcalli/nvim-colorizer.lua'
Plug 'ryanoasis/vim-devicons'
Plug 'kyazdani42/nvim-web-devicons'
" https://github.com/tiagofumo/vim-nerdtree-syntax-highlight/pull/54
Plug 'johnstef99/vim-nerdtree-syntax-highlight', { 'on': 'NERDTreeToggle' }

call plug#end()

" coc extensions
let g:coc_global_extensions = [
  \ 'coc-prettier',
  \ 'coc-java',
  \ 'coc-clojure',
  \ 'coc-pyright',
  \ 'coc-solargraph',
  \ 'coc-xml',
  \ 'coc-yaml',
  \ 'coc-json',
  \ 'coc-eslint',
  \ 'coc-tsserver',
  \ 'coc-deno',
  \ 'coc-vetur',
  \ 'coc-svelte',
  \ 'coc-rust-analyzer',
  \ 'coc-clangd',
  \ 'coc-texlab',
  \ 'coc-esbonio',
  \ 'coc-go',
  \ 'coc-lua',
  \ 'coc-docker',
  \ 'coc-cmake',
  \ 'coc-vimlsp',
  \ 'coc-diagnostic'
  \]
if has('mac')
  let s:llvm_path = system('brew --prefix llvm')
  let s:clangd_path = trim(s:llvm_path).'/bin/clangd'
  call coc#config('clangd.path', s:clangd_path)
endif
if executable('conan')
  let s:conan_path = resolve(resolve(exepath('conan')).'/..')
  let s:conan_py_path = s:conan_path.'/python'
  let s:conan_py_args =
    \ 'import os,site;print(os.linesep.join(site.getsitepackages()))'
  let s:conan_py_site =
    \ split(trim(system(s:conan_py_path.' -c "'.s:conan_py_args.'"')))
  call coc#config('python.analysis.extraPaths', s:conan_py_site)
endif

" telescope extensions
lua << EOF
local telescope = require('telescope')
local actions = require('telescope.actions')
telescope.setup {
  defaults = {
    mappings = {
      i = {
        ['<C-j>'] = actions.move_selection_next,
        ['<C-k>'] = actions.move_selection_previous,
        ['<esc>'] = actions.close,
      },
    },
    path_display = { 'smart' },
    dynamic_preview_title = true,
    vimgrep_arguments = { 'ag', '--vimgrep' },
  },
  extensions = {
    fzf = {
      fuzzy = true,
      override_generic_sorter = true,
      override_file_sorter = true,
      case_mode = 'smart_case',
    },
    file_browser = {
      hijack_netrw = true,
    },
  },
}
telescope.load_extension('fzf')
telescope.load_extension('coc')
telescope.load_extension('file_browser')
EOF

" treesitter extensions
lua << EOF
require 'nvim-treesitter.configs'.setup {
  ensure_installed = {
    'bash', 'beancount', 'c', 'cmake', 'cpp', 'cuda', 'css', 'dockerfile',
    'dot', 'erlang', 'go', 'gomod', 'gowork', 'html', 'java', 'javascript',
    'julia', 'kotlin', 'latex', 'lua', 'make', 'proto', 'python', 'ruby',
    'rust', 'scala', 'scss', 'sql', 'svelte', 'toml', 'tsx', 'typescript',
    'vim', 'yaml',
  },
  sync_install = false,
  highlight = {
    enable = true,
    disable = {
      'proto',
    },
  },
  incremental_selection = {
    enable = true,
    keymaps = {
      init_selection = ",tss",
      scope_incremental = ",tsn",
      node_incremental = ",tnn",
      node_decremental = ",tnp",
    },
  },
}
EOF

" format and user interface
set nofoldenable
set number
set softtabstop=4 tabstop=4 shiftwidth=4
set expandtab
set autoindent
set hlsearch
set mouse=nvc
set cursorline
set laststatus=2
set backspace=2
set colorcolumn=80
set termguicolors

" key mapping
let mapleader=","
inoremap jk <esc>
inoremap Jk <esc>
inoremap JK <esc>
inoremap jK <esc>
inoremap <C-L> <right>
nnoremap 1  ^
nnoremap <leader>tb <cmd>TagbarToggle<cr>
nnoremap <leader>tt <cmd>NERDTreeToggle<cr>
nnoremap <leader>nt <cmd>tabnew<cr>
nnoremap <leader>pc <cmd>pclose<cr>
nnoremap <leader>rn <cmd>set relativenumber<cr>
nnoremap <leader>an <cmd>set norelativenumber<cr>
nnoremap <tab> <cmd>tabn<cr>
nnoremap <esc> <cmd>call coc#float#close_all()<cr>
nnoremap <S-TAB> <cmd>tabp<cr>
nnoremap <C-P> <cmd>Telescope find_files<cr>
nnoremap <leader><C-P> <cmd>Telescope builtin theme=dropdown<cr>
nnoremap <leader><S-P> <cmd>Telescope file_browser theme=ivy<cr>
nnoremap <leader><C-J> <cmd>Telescope jumplist theme=dropdown<cr>
nnoremap <leader><C-T> <cmd>Telescope treesitter theme=dropdown<cr>
nnoremap <leader><C-F> <cmd>Telescope live_grep theme=ivy<cr>
nnoremap <leader>psc <cmd>Telescope coc commands theme=dropdown<cr>
nnoremap <leader>psd <cmd>Telescope coc document_symbols theme=dropdown<cr>
nnoremap <leader>psw <cmd>Telescope coc workspace_symbols theme=dropdown<cr>
nnoremap <leader>psr <cmd>Telescope coc references theme=dropdown<cr>
nnoremap <leader>psi <cmd>Telescope coc implementations theme=dropdown<cr>
nnoremap <leader><tab> <cmd>Telescope telescope-tabs list_tabs<cr>
nnoremap <C-J> <C-W>j<C-W>_
nnoremap <C-K> <C-W>k<C-W>_
nnoremap <C-L> <C-W>l<C-W>_
nnoremap <C-H> <C-W>h<C-W>_
nnoremap [g <plug>(coc-diagnostic-prev)
nnoremap ]g <plug>(coc-diagnostic-next)
nnoremap <leader>gb <cmd>Git blame<cr><C-W>l
nnoremap <leader>gd <plug>(coc-definition)
nnoremap <leader>gh <plug>(coc-declaration)
nnoremap <leader>gy <plug>(coc-type-definition)
nnoremap <leader>gi <plug>(coc-implementation)
nnoremap <leader>gr <plug>(coc-references)
nnoremap <leader>gu <plug>(coc-references-used)
nnoremap <leader>gs <cmd>call CocActionAsync('doHover')<cr>
nnoremap <leader>tgd <cmd>call CocActionAsync('jumpDefinition', 'tabe')<cr>
nnoremap <leader>tgh <cmd>call CocActionAsync('jumpDeclaration', 'tabe')<cr>
nnoremap <leader>tgy <cmd>call CocActionAsync('jumpTypeDefinition', 'tabe')<cr>
nnoremap <leader>tgi <cmd>call CocActionAsync('jumpImplementation', 'tabe')<cr>
nnoremap <leader>tgr <cmd>call CocActionAsync('jumpReferences', 'tabe')<cr>
nnoremap <leader>tgu <cmd>call CocActionAsync('jumpUsed', 'tabe')<cr>
nnoremap <leader>vgd <cmd>call CocActionAsync('jumpDefinition', 'vsplit')<cr>
nnoremap <leader>vgh <cmd>call CocActionAsync('jumpDeclaration', 'vsplit')<cr>
nnoremap <leader>vgy <cmd>call CocActionAsync('jumpTypeDefinition', 'vsplit')<cr>
nnoremap <leader>vgi <cmd>call CocActionAsync('jumpImplementation', 'vsplit')<cr>
nnoremap <leader>vgr <cmd>call CocActionAsync('jumpReferences', 'vsplit')<cr>
nnoremap <leader>vgu <cmd>call CocActionAsync('jumpUsed', 'vsplit')<cr>
nnoremap <leader>rst <cmd>CocRestart<cr>
nnoremap <leader>rn <plug>(coc-rename)
xnoremap <leader>ff <plug>(coc-format-selected)
nnoremap <leader>ff <plug>(coc-format-selected)
nnoremap <leader>cca <plug>(coc-codeaction-line)
xnoremap <leader>cca <plug>(coc-codeaction-selected)
nnoremap <leader>ccl <plug>(coc-codelens-action)
if has('mac')
  nnoremap <leader>D <cmd>DashWord<cr>
else
  nnoremap <leader>D <cmd>Zeavim<cr>
endif
function! CheckIsHeadOfLine() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1] =~# '\s'
endfunction
inoremap <silent><expr> <tab>
  \ coc#pum#visible() ? coc#pum#next(1):
  \ CheckIsHeadOfLine() ? '<tab>' : coc#refresh()
inoremap <silent><expr> <S-tab>
  \ coc#pum#visible() ? coc#pum#prev(1) : coc#refresh()
inoremap <silent><expr> <cr>
  \ coc#pum#visible() ? coc#pum#confirm() :
  \ '<C-g>u<cr><c-r>=coc#on_enter()<cr>'
inoremap <silent><expr> <C-J>
  \ coc#pum#visible() ? coc#pum#next(1) : '<C-J>'
inoremap <silent><expr> <C-K>
  \ coc#pum#visible() ? coc#pum#prev(1) : '<C-K>'
inoremap <silent><expr> <esc>
  \ coc#pum#visible() ? coc#pum#cancel(): '<esc>'

" language specific commands
au filetype * set formatoptions-=o
au filetype *
  \ nmap <leader>H <cmd>CocCommand document.toggleInlayHint<cr>
au filetype rust
  \ nmap <leader>H <cmd>CocCommand rust-analyzer.toggleInlayHints<cr>
au filetype markdown
  \ nmap <leader>P <cmd>MarkdownPreviewToggle<cr>
au User CocJumpPlaceholder
  \ call CocActionAsync('showSignatureHelp')

" custom commands
com! -nargs=0 FormatJSON %!python3 -m json.tool
com! -nargs=0 Format     call CocAction('format')
com! -nargs=0 OR         call CocAction('runCommand', 'editor.action.organizeImport')
com! -nargs=0 Tig        Telescope git_commits theme=dropdown
com! -nargs=0 TigC       Telescope git_bcommits theme=dropdown

" automatic commands
lua require'colorizer'.setup()

" global options
let g:airline_powerline_fonts = 1
let g:airline_theme = 'onedark'
let g:NERDTreeIgnore = [
  \ '\.py[oc]$',
  \ '__pycache__',
  \ '\.egg-info',
  \ '\.[od]$[[file]]'
  \]
let g:NERDTreeStatusline = "NERDTree"
let g:tagbar_ctags_bin='/usr/bin/ctags'
if has('mac')
  let s:ctags_path = system('brew --prefix universal-ctags')
  let g:tagbar_ctags_bin = trim(s:ctags_path).'/bin/ctags'
endif
let g:coc_disable_transparent_cursor = 1 " https://github.com/neoclide/coc.nvim/issues/1775#issuecomment-1086225391

" display style
colorscheme onedark

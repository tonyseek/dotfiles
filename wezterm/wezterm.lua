local wezterm = require 'wezterm'

local config = {}

if wezterm.config_builder then
  config = wezterm.config_builder()
end

config.font = wezterm.font 'FiraCode Nerd Font Mono'
config.font_size = 12.0
config.harfbuzz_features = { 'calt=0', 'clig=0', 'liga=0' }

config.color_scheme = 'OneDark (base16)'
config.hide_tab_bar_if_only_one_tab = true
config.use_fancy_tab_bar = false
config.colors = {
  tab_bar = {
    background = '#282c34',
    active_tab = {
      fg_color = '#abb2bf',
      bg_color = '#353b45',
    },
    inactive_tab = {
      fg_color = '#565c64',
      bg_color = '#282c34',
    },
  },
}
config.colors.tab_bar.inactive_tab_hover = config.colors.tab_bar.active_tab
config.colors.tab_bar.new_tab = config.colors.tab_bar.inactive_tab
config.colors.tab_bar.new_tab_hover = config.colors.tab_bar.active_tab

config.keys = {
  {
    key = 'LeftArrow',
    mods = 'META',
    action = wezterm.action.SendString '\x1bb',
  },
  {
    key = 'RightArrow',
    mods = 'META',
    action = wezterm.action.SendString '\x1bf',
  },
  {
    key = 'Backspace',
    mods = 'META',
    action = wezterm.action.SendString '\x1b\x7f',
  },
  {
    key = 'p',
    mods = 'SUPER',
    action = wezterm.action.ActivateCommandPalette,
  },
  {
    key = 'Enter',
    mods = 'SUPER',
    action = wezterm.action.ToggleFullScreen,
  },
}
config.bypass_mouse_reporting_modifiers = 'META'

config.mouse_bindings = {
  {
    event = { Up = { streak = 1, button = 'Left' } },
    mods = 'NONE',
    action = wezterm.action.Nop,
  },
}

return config

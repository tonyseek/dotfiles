. + input | map_values(
  if type == "string" then
    gsub("@(?<key>\\w+)@"; "\(env[.key])")
  else
    .
  end
) | {"Profiles": [.]}

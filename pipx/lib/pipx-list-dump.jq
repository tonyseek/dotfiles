.venvs | map_values(
  .metadata.injected_packages | select(length > 0) | map_values(
    .package_version
  )
)

# Saved manifests of pipx

## Cheatsheets

### Jupyter Kernels

To check installed kernels: `jupyter kernelspec list`

- Python: `make -C ~/.config/pipx restore` (`jupyter-core` and `jupyter` themselves)
- Julia: `make -C ~/.config/julia restore` (`IJulia`, would be registered by default)
- Go: `make -C ~/.config/go restore` (`gophernotes`, would be registered by [post-install scripts](../go/hooks/github.com/gopherdata/gophernotes/postinst))
- Rust: `make -C ~/.config/rust restore` (`evcxr_jupyter`, need to register manually by `evcxr_jupyter --install`)

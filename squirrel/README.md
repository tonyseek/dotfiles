# Rime

## How to sync userdb on macOS

### Remote to Local

1. Start the Dropbox and make sure the Rime's directory is up-to-date
2. (Optional) Edit the userdb file
3. Stop the Dropbox
4. `make sync-dropbox-fetch`
5. Click "Sync user data" from Rime's menu
6. `make sync-dropbox-strip`
7. Start the Dropbox

### Local to Remote

1. `make sync`
2. Stop the Dropbox
3. Click "Sync user data" from Rime's menu
4. `make sync-dropbox-strip`
5. Start the Dropbox

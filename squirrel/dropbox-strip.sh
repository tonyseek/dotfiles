#!/bin/sh

set -e

dropbox_ignored() {
  file="$1"

  if command -v attr > /dev/null; then
    set -x
    attr -s com.dropbox.ignored -V 1 "$file"
    { set +x; } 2>/dev/null
    return
  fi

  if command -v xattr > /dev/null; then
    set -x
    xattr -w com.dropbox.ignored 1 "$file"
    { set +x; } 2>/dev/null
    return
  fi

  printf 'Unknown OS: %s\n' "$(uname -s)"
  exit 2
}

dropbox_included() {
  file="$1"

  if command -v attr > /dev/null; then
    set -x
    attr -r com.dropbox.ignored "$file" || true
    { set +x; } 2>/dev/null
    return
  fi

  if command -v xattr > /dev/null; then
    set -x
    xattr -d com.dropbox.ignored "$file" || true
    { set +x; } 2>/dev/null
    return
  fi

  printf 'Unknown OS: %s\n' "$(uname -s)"
  exit 2
}

glob_pattern="$1"
working_dir="$2"

fd . -t f -E "$glob_pattern" "$working_dir" | while read -r ignored; do
  dropbox_ignored "$ignored"
done

fd -g "$glob_pattern" "$working_dir" | while read -r included; do
  dropbox_included "$included"
done

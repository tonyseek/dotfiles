#!/bin/sh

set -e

remote_glob="$1"
remote_dir="$2"
local_dir="$3"
local_glob="$4"

userdb_files="$(fd -g "$remote_glob" "$remote_dir")"

set -x

: Make sure the "$remote_glob" files are present
test -n "$userdb_files"
{ set +x; } 2>/dev/null

printf '%s\n' "$userdb_files" | while read -r userdb_file; do
  set -x
  test -f "$userdb_file"
  { set +x; } 2>/dev/null
done

set -x
: Delete ignored files from remote
{ set +x; } 2>/dev/null

fd . -t f -E "$remote_glob" "$remote_dir" | while read -r ignored; do
  if test -f "$ignored"; then
    set -x
    trash "$ignored"
    { set +x; } 2>/dev/null
  fi
done

set -x
: Delete userdb from local
{ set +x; } 2>/dev/null

fd -g "$local_glob" -t d "$local_dir" | while read -r local_userdb_dir; do
  set -x
  trash "$local_userdb_dir"
  { set +x; } 2>/dev/null
done

printf >&2 '=> Now sync from Rime menu and "make sync-dropbox-strip"\n'

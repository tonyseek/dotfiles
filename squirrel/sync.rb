#!/usr/bin/env ruby

require 'yaml'

f = YAML.load_file(ARGV[0])
f['sync_dir'] = ARGV[1]
File.write(ARGV[0], f.to_yaml)

UNAME := $(shell uname)

ifeq ($(UNAME), Darwin)
	SYMLINK_ARGS = -sfh
else
	SYMLINK_ARGS = -sf
endif

help:
	@echo "all"
	@echo "link-all"
	@echo "link-zshrc"
	@echo "link-tigrc"
	@echo "link-gitconfig"
	@echo "link-tmux"
	@echo "link-ansible"
	@echo "link-gnupg"
	@echo "link-vscodium"
	@echo "link-intellij"
	@echo "vendor-rustup"
.PHONY: help

all: link-all
.PHONY: all

link-all: link-zshrc link-tigrc link-gitconfig link-tmux link-ansible link-gnupg link-fetchmail link-vscodium link-intellij
.PHONY: link-all

link-zshrc: zshrc/zshrc zshrc/zshenv
	mkdir -p $(HOME)/.antigen
	ln $(SYMLINK_ARGS) $(HOME)/.config/zshrc/zshrc $(HOME)/.zshrc
	ln $(SYMLINK_ARGS) $(HOME)/.config/zshrc/zshenv $(HOME)/.zshenv
	ln $(SYMLINK_ARGS) $(HOME)/.config/zshrc/antigen.zsh $(HOME)/.antigen/antigen.zsh
.PHONY: link-zshrc

link-tigrc:
	ln $(SYMLINK_ARGS) $(HOME)/.config/tigrc/tigrc $(HOME)/.tigrc
.PHONY: link-tigrc

link-gitconfig: gitconfig/gitconfig
	ln $(SYMLINK_ARGS) $(HOME)/.config/gitconfig/gitconfig $(HOME)/.gitconfig
.PHONY: link-gitconfig

link-tmux: tmux/tmux.conf
	mkdir -p $(HOME)/.tmux/plugins
	[ -e $(HOME)/.tmux/plugins/tpm ] || git clone https://github.com/tmux-plugins/tpm.git $(HOME)/.tmux/plugins/tpm
	ln $(SYMLINK_ARGS) $(HOME)/.config/tmux/tmux.conf $(HOME)/.tmux.conf
.PHONY: link-tmux

link-ansible: ansible/ansible.cfg
	ln $(SYMLINK_ARGS) $(HOME)/.config/ansible/ansible.cfg $(HOME)/.ansible.cfg
.PHONY: link-ansible

link-gnupg: gnupg/gpg.conf gnupg/gpg-agent.conf
	ln $(SYMLINK_ARGS) $(HOME)/.config/gnupg/gpg.conf $(HOME)/.gnupg/gpg.conf
	ln $(SYMLINK_ARGS) $(HOME)/.config/gnupg/gpg-agent.conf $(HOME)/.gnupg/gpg-agent.conf
.PHONY: link-gnupg

link-fetchmail: fetchmail/fetchmailrc fetchmail/procmailrc
	ln $(SYMLINK_ARGS) $(HOME)/.config/fetchmail/fetchmailrc $(HOME)/.fetchmailrc
	ln $(SYMLINK_ARGS) $(HOME)/.config/fetchmail/procmailrc $(HOME)/.procmailrc
.PHONY: link-fetchmail

link-vscodium:
ifeq ($(UNAME), Darwin)
	mkdir -p $(HOME)/Library/Application\ Support/VSCodium/User
	trash -F $(HOME)/Library/Application\ Support/VSCodium/User
	mkdir -p $(HOME)/Library/Application\ Support/VSCodium/User
	ln $(SYMLINK_ARGS) $(HOME)/.config/vscodium/settings.json $(HOME)/Library/Application\ Support/VSCodium/User/settings.json
	ln $(SYMLINK_ARGS) $(HOME)/.config/vscodium/keybindings.json $(HOME)/Library/Application\ Support/VSCodium/User/keybindings.json
	ln $(SYMLINK_ARGS) $(HOME)/.config/vscodium/snippets $(HOME)/Library/Application\ Support/VSCodium/User/snippets
endif
.PHONY: link-vscodium

link-intellij:
	ln $(SYMLINK_ARGS) $(HOME)/.config/nvim/idea.vim $(HOME)/.ideavimrc
.PHONY: link-intellij

vendor-rustup:
	rustup completions zsh > ./vendor/_rustup
.PHONY: vendor-rustup

# Locale
export LC_ALL=en_US.UTF-8
export LANG=en_US.UTF-8

# Preferred Editor
export EDITOR='vim'

# Manual Path
export MANPATH="/usr/local/man:/opt/homebrew/share/man:$MANPATH"

# User Path
export USER_PATH="$HOME/.local/bin:$HOME/.cargo/bin:$POSTGRES_PATH/bin"

# Android SDK
if [ -d "$HOME/Library/Android/sdk" ]; then
    export ANDROID_HOME="$HOME/Library/Android/sdk"
    export ANDROID_NDK_HOME="$HOME/Library/Android/ndk"
    export ANDROID_AVD_HOME="$HOME/Library/Android/avd"
fi
if [ -n "$ANDROID_HOME" ]; then
    export ANDROID_SDK_ROOT="$ANDROID_HOME"
    export USER_PATH="$USER_PATH:$ANDROID_HOME/platform-tools"
fi

# Ansible
export ANSIBLE_VAULT_PASSWORD_FILE="$HOME/.vault_password"

# Java
if [ -d "/Library/Java/Home" ]; then
    export JAVA_HOME="/Library/Java/Home"
fi

# Python
export PYTHONSTARTUP="$HOME/.config/pythonrc/pythonstartup.py"

# Ruby
export GEM_HOME="$HOME/.local/share/gem"

# Go
export GOPATH=$HOME/.gopath

# Scala
export SBT_OPTS="-XX:+CMSClassUnloadingEnabled -XX:MaxPermSize=256M"

# Perl
export PERL5LIB="$HOME/.local/lib/perl5${PERL5LIB+:}${PERL5LIB}"
export PERL_LOCAL_LIB_ROOT="$HOME/.local/${PERL_LOCAL_LIB_ROOT+:}${PERL_LOCAL_LIB_ROOT}"
export PERL_MB_OPT="--install_base \"$HOME/.local\""
export PERL_MM_OPT="INSTALL_BASE=$HOME/.local"

# BAT (https://github.com/sharkdp/bat)
export BAT_CONFIG_PATH="$HOME/.config/bat/config"
export BAT_PAGER="$HOME/.config/bat/bat-pager.perl"

# The Silver Searcher
rg_with_pager() {
  "$BAT_PAGER" < <(command rg "$@")
}
alias rg="rg_with_pager -p"
alias ag="ag --pager='$BAT_PAGER'"

# FZF (https://github.com/junegunn/fzf)
export FZF_DEFAULT_COMMAND='fd --type f'
export FZF_DEFAULT_OPTS="--preview-window 'right:60%' --preview 'bat --color=always --style=numbers,changes --line-range :500 {}'"
export FZF_COMPLETION_OPTS="--preview-window 'right:hidden'"
export FZF_CTRL_R_OPTS="$FZF_COMPLETION_OPTS"

[ -f "$HOME/.local/etc/zshenv" ] && source "$HOME/.local/etc/zshenv"

#!/usr/bin/env bash

set -e
set -o pipefail

RFC_ID="$1"
if ! grep -qE '^[[:digit:]]+$' <<<"$RFC_ID"; then
    printf >&2 'Usage: %s RFC_ID\n' "$(basename "$0")"
    exit 2
fi

METADATA_URL="https://xml2rfc.tools.ietf.org/public/rfc/bibxml/reference.RFC.${RFC_ID}.xml"
PDF_URL="https://tools.ietf.org/pdf/rfc${RFC_ID}.pdf"

printf >&2 '=> Downloading metadata\n'
METADATA_TITLE="$(wget -O - "$METADATA_URL" | xmllint --xpath '//reference/front/title/text()' -)"

printf >&2 '=> Downloading PDF\n'
PDF_FILENAME="RFC${RFC_ID}-$(sed 's/[^a-zA-Z0-9.]/-/g;s/--*/-/g' <<< "$METADATA_TITLE").pdf"
wget -c -O "$PDF_FILENAME" "$PDF_URL"

printf '%s\n' "$PDF_FILENAME"

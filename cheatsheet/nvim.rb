cheatsheet do
  title "tonyseek's nvim"
  docset_file_name 'tonyseek-nvim'
  keyword 'nvim'

  introduction "The neovim cheat sheet from **tonyseek's dotfiles**"

  category do
    id 'Panels (Normal Mode)'
    entry do
      command ',+tb'
      name 'Toggle TagBar'
    end
    entry do
      command ',+tt'
      name 'Toggle NERDTree'
    end
    entry do
      command ',+nt'
      name 'New Tab'
    end
    entry do
      command ',+pc'
      name 'Preview Close'
    end
    entry do
      command ',+rn'
      name 'Enable relative number and disable absolute number'
    end
    entry do
      command ',+an'
      name 'Enable absolute number and disable relative number'
    end
  end

  category do
    id 'Telescope (Normal Mode)'
    entry do
      command 'CTRL+P'
      name 'Search files for path pattern'
    end
    entry do
      command ',+CTRL+P'
      name 'Search for buildtin commands of Telescope'
    end
    entry do
      command ',+Shift+P'
      name 'Open file browser'
    end
    entry do
      command ',+CTRL+J'
      name 'Search jumplist of previous footprint'
    end
    entry do
      command ',+CTRL+T'
      name 'Search for code symbols from current opened file by TreeSitter'
    end
    entry do
      command ',+CTRL+F'
      name 'Search for symbols from current directory (Ag)'
    end
    entry do
      command ',<tab>'
      name 'Search for opened tabs'
    end
  end

  category do
    id 'Document LSP (Normal Mode)'
    entry do
      command ',psc'
      name 'Search for coc.nvim commands'
    end
    entry do
      command ',psd'
      name 'Search for symbols in document scope'
    end
    entry do
      command ',psw'
      name 'Search for symbols in workspace scope'
    end
    entry do
      command ',psr'
      name 'Search for references of cursor-focused keyword'
    end
    entry do
      command ',psi'
      name 'Search for implementations of cursor-focused keyword'
    end
  end

  category do
    id 'Diagnostics (Normal Mode)'
    entry do
      command '[g'
      name 'Jump to the next diagnostic issue'
    end
    entry do
      command ']g'
      name 'Jump to the previous diagnostic issue'
    end
  end

  category do
    id 'Navigation LSP (Normal Mode)'
    entry do
      command ',gb'
      name 'Open or refresh the "git blame" splited-window'
    end
    entry do
      command ',gd'
      name 'Go to definition'
    end
    entry do
      command ',gh'
      name 'Go to declaration (header)'
    end
    entry do
      command ',gy'
      name 'Go to type definition'
    end
    entry do
      command ',gi'
      name 'Go to implementation'
    end
    entry do
      command ',gr'
      name 'Go to references'
    end
    entry do
      command ',gu'
      name 'Go to used references (declaration excluded)'
    end
    entry do
      command ',tg[dhyiru]'
      name 'Go to somewhere as above in a new tab'
    end
    entry do
      command ',vg[dhyiru]'
      name 'Go to somewhere as above in a vertical split pannel'
    end
    entry do
      command ',rst'
      name 'Restart the language server'
    end
    entry do
      command ',D'
      name 'Search for keyword on Dash or Zeal'
    end
  end

  category do
    id 'Refactor LSP (Normal Mode)'
    entry do
      command ',rn'
      name 'Rename identity'
    end
    entry do
      command ',ff'
      name 'Format focused-or-selected lines'
    end
    entry do
      command ',cca'
      name 'Show code actions for focused-or-selected lines'
    end
    entry do
      command ',ccl'
      name 'Show code lens for focused-or-selected lines'
    end
  end

  category do
    id 'Custom Commands'
    entry do
      command ':FormatJSON'
      name 'Format current document with Python JSON parser'
    end
    entry do
      command ':Format'
      name 'Format current document with LSP'
    end
    entry do
      command ':OR'
      name 'Organize import statements'
    end
    entry do
      command ':Tig'
      name 'Show Git commits of current directory'
      notes 'Press ENTER to checkout to selected commit'
    end
    entry do
      command ':TigC'
      name 'Show Git commits of current file'
      notes 'Press ENTER to checkout and reopen file to selected commit'
    end
  end
end

__import__('rlcompleter')
__import__('readline').parse_and_bind('tab:complete')

try:
    import numpy as np
except ImportError:
    pass
else:
    if getattr(np, '__version__', None):
        print('%-10s-> numpy %s' % ('np', np.__version__))

try:
    import scipy as sp
except ImportError:
    pass
else:
    if getattr(sp, '__version__', None):
        print('%-10s-> scipy %s' % ('sp', sp.__version__))

try:
    import matplotlib
except ImportError:
    pass
else:
    class pyplot(object):
        __version__ = getattr(matplotlib, '__version__', None)

        def __init__(self):
            self.pyplot = None

        def __getattribute__(self, name):
            if name in ('pyplot', '__version__'):
                return object.__getattribute__(self, name)
            if self.pyplot is None:
                self.pyplot = __import__(
                    'matplotlib.pyplot', fromlist=('matplotlib',))
            return getattr(self.pyplot, name)

    if pyplot.__version__:
        global plt
        plt = pyplot()
        print('%-10s-> matplotlib.pyplot %s' % ('plt', plt.__version__))

try:
    import pandas as pd
except ImportError:
    pass
else:
    if getattr(pd, '__version__', None):
        print('%-10s-> pandas %s' % ('pd', pd.__version__))

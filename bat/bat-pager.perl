#!/usr/bin/env perl

# Upgrade less to 530 at least if you want to enable mouse scrolling
if (qx(less -V) =~ /less (\d+)/ && $1 >= 530) {
  exec "less --tabs=4 -RF" or print STDERR "exec less failed: $!"
} else {
  exec "less --tabs=4 -RFX" or print STDERR "exec less failed: $!"
}
